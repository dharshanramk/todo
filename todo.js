const express = require("express");
const bodyParser = require("body-parser");
var fs = require("fs");
const app = express();

app.use(bodyParser.urlencoded({
    extended:true
}));

app.get("/",function(req,res){
    res.sendFile(__dirname + "/index.html");
})
app.post("/addTask",function(req,res){
    var task1 = req.body.task1;
    var task2 = req.body.task2;
    var task3 = req.body.task3;
    var task4 = req.body.task4;
    var task5 = req.body.task5;
    var obj = {};
    var key = req.body.dayname;
    var newtask = {
        "Taskname1" : task1,
        "Taskname2" : task2,
        "Taskname3" : task3,
        "Taskname4" : task4,
        "Taskname5" : task5,

    }
    obj[key] = newtask;

fs.readFile("todo.json","utf8",function(err,data){
    data = JSON.parse(data);
    data[key] = obj[key];
    console.log(data);
    var updatetask = JSON.stringify(data);
    fs.writeFile("todo.json",updatetask,function(err)
    {
        res.end(JSON.stringify(data));
    });
   });
});

app.listen(3000,function(){
    console.log("Click this link to add new task : http://localhost:3000");
    console.log("Server is running on port 3000");
});